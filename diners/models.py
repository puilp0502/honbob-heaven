from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import Avg
from django.db.models.signals import post_save
from django.dispatch import receiver


class Feature(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Restaurant(models.Model):
    KOREAN = 'KOR'
    WESTERN = 'WES'
    CHINESE = 'CHN'
    JAPANESE = 'JPN'
    GLOBAL = 'GLO'
    SNACK = 'SNK'
    FASTFOOD = 'FST'
    BAR = 'BAR'
    OTHER = 'OTH'
    CATEGORY_CHOICES = (
        (KOREAN, '한식'),
        (WESTERN, '양식'),
        (CHINESE, '중식'),
        (JAPANESE, '일식'),
        (GLOBAL, '세계음식'),
        (SNACK, '분식'),
	(FASTFOOD, '패스트푸드'),
        (BAR, '주점'),
        (OTHER, '기타'),
    )
    name = models.CharField(max_length=64)
    address = models.CharField(max_length=128, blank=True)
    description = models.TextField(blank=True)
    telephone = models.CharField(max_length=16, blank=True)
    category = models.CharField(max_length=3, choices=CATEGORY_CHOICES, default=OTHER, blank=False)
    features = models.ManyToManyField(Feature)
    pos = models.PointField(geography=True, help_text='Use POINT(lng lat) format')
    image = models.ImageField()
    level = models.PositiveIntegerField('Honbob difficulty', help_text='from 1 to 3', default=1)

    taste_score = models.FloatField('맛 평점 평균', default=0, help_text='from 1 to 3')
    mood_score = models.FloatField('분위기 평점 평균', default=0, help_text='from 1 to 3')
    qty_score = models.FloatField('양 평점 평균', default=0, help_text='from 1 to 3')

    def __str__(self):
        return "Restaurant " + str(self.name)


class Review(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    writer = models.ForeignKey(User, on_delete=models.CASCADE)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, related_name='reviews')

    picture = models.ImageField()
    contents = models.TextField()

    taste_score = models.PositiveIntegerField('맛 평점', default=2, help_text='from 1 to 3')
    mood_score = models.PositiveIntegerField('분위기 평점', default=2, help_text='from 1 to 3')
    qty_score = models.PositiveIntegerField('양 평점', default=2, help_text='from 1 to 3')

    def __str__(self):
        return "Review at "+self.restaurant.name+" from "+self.writer.username


@receiver(post_save, sender=Review)
def update_restaurant_score(sender, instance=None, created=False, **kwargs):
    """
    Update restaurant's average score
    """
    restaurant = instance.restaurant
    average_values = Review.objects.filter(restaurant=restaurant).aggregate(Avg('taste_score'),
                                                                            Avg('mood_score'),
                                                                            Avg('qty_score'))
    restaurant.taste_score = average_values['taste_score__avg']
    restaurant.mood_score = average_values['mood_score__avg']
    restaurant.qty_score = average_values['qty_score__avg']
    restaurant.save()
