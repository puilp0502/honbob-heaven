from django.core.cache import cache

from .models import Restaurant, Feature, Review
from rest_framework import serializers
from rest_framework_extensions.mixins import NestedViewSetMixin
from honbob.serializers import RawUserSerializer


class ModelSerializerCacheMixin(object):
    def to_representation(self, instance):
        cache_name = instance.__class__.__name__+str(instance.pk)
        cached_value = cache.get(cache_name)
        if cached_value is None:
            print('Cache for '+cache_name+' does not exist; populating...')
            cached_value = super(ModelSerializerCacheMixin, self).to_representation(instance)
            cache.set(cache_name, cached_value, None)
        else:
            print('Using cached value of '+cache_name)
        return cached_value


class PositionSerializer(serializers.Serializer):
    lat = serializers.FloatField(source='y')
    lng = serializers.FloatField(source='x')


class FeatureSerializer(ModelSerializerCacheMixin, serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Feature
        fields = ('url', 'name')


class RestaurantSerializer(ModelSerializerCacheMixin, NestedViewSetMixin, serializers.HyperlinkedModelSerializer):
    features = FeatureSerializer(many=True)
    pos = PositionSerializer()
    dist = serializers.CharField()

    class Meta:
        model = Restaurant
        fields = ('url', 'category', 'name', 'address', 'description', 'telephone', 'features', 'pos', 'image', 'dist',
                  'taste_score', 'mood_score', 'qty_score')


def score_validator(value):
    if not 1 <= value <= 3:
        raise serializers.ValidationError('Score must be an integer between 1 and 3')


class ReviewSerializer(NestedViewSetMixin, serializers.HyperlinkedModelSerializer):
    writer = RawUserSerializer(read_only=True, default=serializers.CurrentUserDefault())
    taste_score = serializers.IntegerField(validators=[score_validator])
    mood_score = serializers.IntegerField(validators=[score_validator])
    qty_score = serializers.IntegerField(validators=[score_validator])

    class Meta:
        model = Review
        fields = ('url', 'created_at', 'writer', 'restaurant', 'picture', 'contents',
                  'taste_score', 'mood_score', 'qty_score')
