from datetime import timedelta

from django.db.models import Case
from django.db.models import Count, Sum
from django.contrib.auth.models import User
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.db.models import IntegerField
from django.db.models import When
from django.utils import timezone
from rest_framework import viewsets
from rest_framework import filters
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework_extensions.mixins import NestedViewSetMixin

from honbob.serializers import UserSerializer
from .serializers import RestaurantSerializer, FeatureSerializer, ReviewSerializer
from .models import Restaurant, Feature, Review
from .permissions import IsOwnerOrReadOnly


class RestaurantViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = RestaurantSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('name', 'dist')
    ordering = ('dist',)

    def get_queryset(self):
        # Try to get user's latitude & longitude. Defaults to City Hall.
        lat = float(self.request.query_params.get('lat', '37.565756'))
        lng = float(self.request.query_params.get('lng', '126.977099'))
        radius = int(self.request.query_params.get('radius', 100000))
        current = Point(lng, lat, srid=4326)
        queryset = Restaurant.objects.annotate(dist=Distance('pos', current)).filter(dist__lte=radius)
        try:
            features = self.request.query_params['features'].split(',')
            # Restaurants which have at least one of the features
            queryset = queryset.filter(features__name__in=features)\
                    .annotate(f_count=Count('features')).filter(f_count=len(features))  # Have all the features
        except KeyError:
            pass
        try:
            categories = self.request.query_params['category'].split(',')
            queryset = queryset.filter(category__in=categories)
        except KeyError:
            pass
        return queryset.prefetch_related('features', 'reviews')


class FeatureViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Feature.objects.all()
    serializer_class = FeatureSerializer


class ReviewViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, )
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer


@api_view(['GET'])
def ranking(request):
    today = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
    weekday = today.weekday()
    first_day = today - timedelta(days=weekday)
    last_day = first_day + timedelta(days=7)
    queryset = User.objects.annotate(score=Sum(
        Case(When(review__created_at__gte=first_day,
                  review__created_at__lt=last_day,
                  then='review__restaurant__level'), output_field=IntegerField())
    )).exclude(score=None).order_by('-score')[:50]
    serialized = UserSerializer(queryset, many=True)
    return Response(serialized.data)
