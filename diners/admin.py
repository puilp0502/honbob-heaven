from django.contrib import admin
from django.forms.widgets import TextInput
from django.contrib.gis.db import models
from .models import (
    Feature,
    Restaurant,
    Review,
)


def rename(newname):
    def decorator(f):
        f.__name__ = newname
        return f
    return decorator


def change_category_factory(category_name, category_value):
    @rename(category_name)
    def change_category(modeladmin, request, queryset):
        queryset.update(category=category_value)
    change_category.short_description = "Change category to "+category_name
    return change_category


class TextboxGeoAdmin(admin.ModelAdmin):
    
    formfield_overrides = {
        models.PointField: {'widget': TextInput }
    }
    actions = [change_category_factory(name, value) for value, name in Restaurant.CATEGORY_CHOICES]
    search_fields = ['name', 'address', 'telephone']

admin.site.register(Feature)
admin.site.register(Review)
admin.site.register(Restaurant, TextboxGeoAdmin)
