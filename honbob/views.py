from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http import JsonResponse
from requests import HTTPError
from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.response import Response
from social.apps.django_app.utils import psa

from .serializers import UserSerializer, UserWriteSerializer


class UserViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = User.objects.annotate(score=Sum('review__restaurant__level'))
    serializer_class = UserSerializer


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


@psa('social:complete')
def login_by_token(request, backend):
    token = request.GET.get('access_token', '')
    try:
        user = request.backend.do_auth(token)
        if user and user.is_active:
            token = Token.objects.get_or_create(user=user)[0]
            print(token)
            result = {'token': token.key}
            return JsonResponse(result)
        else:
            result = {'message': 'Failed to authenticate.', 'result': 'fail'}
            return JsonResponse(result, status=400)
    except HTTPError as e:
        print(e.response.text)
        result = {'message': 'Failed to authenticate. Auth Provider refused to authenticate.', 'result': 'fail'}
        return JsonResponse(result, status=e.response.status_code)


@api_view(['GET'])
def destroy_auth_token(request):
    if not request.user:
        result = {'message': 'Not logged in', 'result': 'fail'}
        return Response(result, status=status.HTTP_400_BAD_REQUEST)
    try:
        Token.objects.get(user=request.user).delete()
    except ObjectDoesNotExist:  # Token has not been created; no action required
        pass
    result = {'message': 'Token destroyed.', 'result': 'ok'}
    return Response(result)


@api_view(['POST'])
def register(request):
    serialized = UserWriteSerializer(data=request.data)
    if serialized.is_valid():
        user = User.objects.create_user(
            serialized.data['username'],
            email=serialized.data.get('email'),
        )
        user.set_password(serialized.data['password'])
        user.save()
        return Response({'Token': Token.objects.get(user=user).key}, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def me(request):
    if request.user.is_authenticated():
        user = User.objects.filter(pk=request.user.pk).annotate(score=Sum('review__restaurant__level'))[0]
        serialized = UserSerializer(user)
        return Response(serialized.data)
    else:
        result = {'message': 'Not logged in', 'result': 'fail'}
        return Response(result, status=status.HTTP_400_BAD_REQUEST)
