"""honbob URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework.authtoken import views as token_views
from rest_framework_extensions import routers

from diners import views as diners_views
from honbob import views as root_views
from honbob.settings import base as settings

router = routers.ExtendedSimpleRouter()
router.register(r'restaurants', diners_views.RestaurantViewSet, base_name='restaurant')\
    .register(r'reviews', diners_views.ReviewViewSet, base_name='review', parents_query_lookups=['restaurant'])
router.register(r'features', diners_views.FeatureViewSet)
router.register(r'users', root_views.UserViewSet, base_name='user')\
    .register(r'reviews', diners_views.ReviewViewSet, base_name='review', parents_query_lookups=['writer'])
router.register(r'reviews', diners_views.ReviewViewSet, base_name='review')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^ranking/', diners_views.ranking),
    url(r'^auth/me/', root_views.me),
    url(r'^auth/join/', root_views.register),
    url(r'^auth/login/', token_views.obtain_auth_token),
    url(r'^auth/logout/', root_views.destroy_auth_token),
    url(r'^oauth/(?P<backend>[^/]+)/$', root_views.login_by_token),
    url(r'^socialauth/', include('social.apps.django_app.urls', namespace='social')),
    url(r'^webauth/', include('rest_framework.urls', namespace='auth')),
    url(r'^admin/', admin.site.urls),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
