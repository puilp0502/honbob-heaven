from django.contrib.auth.models import User
from rest_framework import serializers


class RawUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('pk', 'username')


class UserSerializer(serializers.ModelSerializer):
    score = serializers.IntegerField()

    class Meta:
        model = User
        fields = ('pk', 'username', 'score',)


class UserWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
