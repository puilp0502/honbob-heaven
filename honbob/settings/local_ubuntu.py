from honbob.settings.base import *  # NOQA

DEBUG = True
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'circle_test',
        'USER': 'ubuntu',
    }
}
INSTALLED_APPS += ['debug_toolbar']
