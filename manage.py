#!/usr/bin/env python
import os
import sys
from getpass import getuser

if __name__ == "__main__":
    
    os.environ["DJANGO_SETTINGS_MODULE"] = "honbob.settings.local_{}".format(getuser())
    print('Trying to use', os.environ['DJANGO_SETTINGS_MODULE'])
    from django.core.management import execute_from_command_line
    try:
        execute_from_command_line(sys.argv)
    except ImportError as e:
        print('Failed; falling back to production settings:', e.__str__())
        os.environ['DJANGO_SETTINGS_MODULE'] = 'honbob.settings.production'
        import django
        django.setup()
        execute_from_command_line(sys.argv)
